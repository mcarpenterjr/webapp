# Documentation


## Users

 - [FAQ](./faq.md)
 - [Gitter Accounts](./accounts.md)
 - [Rooms](./rooms.md)
 - [Communities](./communities.md)
 - [Notifications](./notifications.md)
 - [OAuth Scopes](./oauth-scopes.md)


## Developers

 - [Code overview](./code-overview.md)
 - [Getting started with production](./getting-started-production.md)
